const moment         = require("moment")
const {budget}       = require("../lib/util")
const express        = require("express")
const router         = express.Router()
router.post("/api/v1/budget", (req, res) => {
  let {startDate,numberOfDays} = req.body
  const start        = moment(startDate, "MM/DD/YYYY")
  if (!start.isValid()) {
    res.status(500).json({error:"invalid startDate"})
  } else {
    let totalCost    = budget(start, numberOfDays)
    res.status(200).json({totalCost:totalCost})
  }
})
module.exports       = router
