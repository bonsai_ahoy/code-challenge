const router     = require("./app/routes")
const express    = require("express")
const bodyParser = require("body-parser")
const app        = express()
const port       = process.env.PORT ? process.env.PORT : 3000
app.use(bodyParser.json())
app.use(router)
app.get("*",  (req, res) => res.status(404).send())
app.post("*", (req, res) => res.status(404).send())
let server       = app.listen(port, "127.0.0.1", () => console.log(`listening 127.0.0.1:${port}`))
function appStop() { server.close() }
module.exports   = {app,appStop}

