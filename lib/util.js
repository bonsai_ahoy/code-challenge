const sprintf     = require("sprintfjs")
const Moment      = require("moment")
const MomentRange = require("moment-range")
const moment      = MomentRange.extendMoment(Moment)
moment.updateLocale("en", { week: { dow: 1 }}) // set first day of week to Monday for easier skipping over weekends
function budget(start, numberOfDays) {
  let   stopDay   = numberOfDays - 1; // subtract 1 to include start date in range
  const stop      = start.clone().add(stopDay, "d")
  const range     = moment.range(start, stop)
  let total       = 0.00
  for (let day of range.by("day")) {
    if (day.weekday() > 4) continue; // skip weekends
    let week      = Math.ceil(day.date()/7)
    let cost      = 0.00
    switch(week) {
      case 1:  cost += 0.05 //  first  7 days of month cost $0.05
        break
      case 2:  cost += 0.10 //  second 7 days of month cost $0.10
        break
      case 3:  cost += 0.15 //  third  7 days of month cost $0.15
        break
      case 4:  cost += 0.20 //  fourth 7 days of month cost $0.20
        break
      default: cost += 0.25 //  further  days of month cost $0.25
    }
    total += cost
    //if (process.env.NODE_ENV === "development") {
    //  console.log(sprintf("%10s week:%3d cost:%6.2f total:%6.2f", day.format("MM/DD/YYYY"), week, cost, total))
    //}
  }
  return total.toFixed(2) // JSON.stringify automatically converts whole number floats to ints. 
                          // call toFixed() so that "1.00" isn't converted to "1"
}
module.exports = {budget}
