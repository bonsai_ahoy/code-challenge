const assert        = require("assert")
const request       = require("supertest")
const {app,appStop} = require("../index")
let endpoint        = "/api/v1/budget" 
let mockData        = {
  "07/01/2019"      : {
    numDays         : 6,
    cost            : 0.05,
    totalCost       : 0.25
  },
  "07/08/2019"      : {
    numDays         : 6,
    cost            : 0.10,
    totalCost       : 0.50
  },
  "07/15/2019"      : {
    numDays         : 6,
    cost            : 0.15,
    totalCost       : 0.75,
  },
  "07/22/2019"      : {
    numDays         : 6,
    cost            : 0.20,
    totalCost       : 1.00,
  },
  "07/29/2019"      : {
    numDays         : 3,
    cost            : 0.25,
    totalCost       : 0.75
  }
}
function testStartDate(startDate, numberOfDays, totalCost, done) {
  request(app)
    .post(endpoint)
    .set("Content-Type", "application/json")
    .set("Accept"      , "application/json")
    .send({startDate:startDate, numberOfDays:numberOfDays})
    .expect("Content-Type", /json/)
    .expect(200)
    .end(function(err, res) {
      if (err) return done(err)
      assert.equal(Number(res.body.totalCost), totalCost)
      done();
    })
}
describe("Budget API Endpoint", function () {
  describe(`POST ${endpoint}`, function() {
    Object.keys(mockData).forEach((startDate, idx) => {
      let week      = idx+1
      let cost      = mockData[startDate].cost
      let totalCost = mockData[startDate].totalCost
      let numDays   = mockData[startDate].numDays
      it(`totalCost for days:1 starting:${startDate} should equal ${cost}`, function (done) {
        testStartDate(startDate, 1, cost, done)
      })
      it(`totalCost for days:${numDays} starting:${startDate} should equal ${totalCost}`, function (done) {
        testStartDate(startDate, numDays, totalCost, done)
      })
    })
    it(`totalCost for the month starting:07/01/2019 should equal 3.25`, function (done) {
      testStartDate("07/01/2019", 31, 3.25, done)
    })
    it(`totalCost for the year starting:01/01/2019 should equal 35.25`, function (done) {
      testStartDate("01/01/2019", 365, 35.25, done)
    })
    after(async () => {
      appStop()
    })
  })
})
